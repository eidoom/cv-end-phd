DOCNAME=cv
SRC=$(DOCNAME).tex $(DOCNAME).bib $(DOCNAME).cls

PUBNAME=pubs
PUB=$(PUBNAME).tex $(DOCNAME).bib $(DOCNAME).cls

all: $(DOCNAME).pdf $(PUBNAME).pdf

.PHONY: clean cont view

$(DOCNAME).pdf: $(SRC)
	latexmk -xelatex $<

cont: $(SRC)
	ls $^ | entr make

view: $(DOCNAME).pdf
	xdg-open $< &

$(PUBNAME).pdf: $(PUB)
	latexmk -xelatex $<

clean:
	-rm *.blg *.bbl *.aux *.log *.out *.toc

