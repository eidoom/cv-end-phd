# [cv-end-phd](https://gitlab.com/eidoom/cv-end-phd)

Live here:
* [CV](http://eidoom.gitlab.io/cv-end-phd/cv.pdf)
* [Publications](http://eidoom.gitlab.io/cv-end-phd/pubs.pdf)
