\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cv}[2021-11-05 Ryan's custom CV class]

\LoadClass[10pt, a4paper]{article}

\usepackage[hmargin=15mm, vmargin=15mm]{geometry}
\usepackage[maxbibnames=99,sorting=ymnt]{biblatex}
\usepackage{tabularx,xunicode,xltxtra,url,parskip,fontspec,hyperref,titlesec,multicol,color,graphicx}

\addbibresource{cv.bib}

\DeclareSortingTemplate{ymnt}{
  \sort{
    \field{presort}
  }
  \sort[final]{
    \field{sortkey}
  }
  \sort[direction=descending]{
    \field{sortyear}
    \field{year}
    \literal{0000}
  }
  \sort[direction=descending]{
    \field[padside=left,padwidth=2,padchar=0]{month}
    \literal{00}
  }
  \sort{
    \field{sortname}
    \field{author}
    \field{editor}
    \field{translator}
  }
  \sort{
    \field{sorttitle}
    \field{title}
  }
}

% Headings format
\titleformat{\section}{\Large\raggedright\heavy}{}{0em}{}[\titlerule]
\titlespacing{\section}{0pt}{3pt}{2ex}

% Link colour
\definecolor{linkcolour}{rgb}{0,0.2,0.6}
\hypersetup{colorlinks,breaklinks,urlcolor=linkcolour,linkcolor=linkcolour}

% Custom table column formats
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{Y}{>{\centering\arraybackslash}X}
\newcolumntype{Z}{>{\raggedright\arraybackslash}X}

% Custom font
\defaultfontfeatures{Ligatures=TeX}

\setmainfont[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Semibold.otf,
  ItalicFont = SourceSansPro-LightIt.otf,
  BoldItalicFont = SourceSansPro-SemiboldIt.otf
]{SourceSansPro-Light.otf}

\newfontfamily\heavy[
  Path=fonts/,
  Ligatures=TeX,
  BoldFont = SourceSansPro-Bold.otf,
  ItalicFont = SourceSansPro-It.otf,
  BoldItalicFont = SourceSansPro-BoldIt.otf
]{SourceSansPro-Regular.otf}

\font\default=''[cmr10]''

\hyphenpenalty=10000
